/**
 * @author      : oli (oli@$HOSTNAME)
 * @file        : main
 * @created     : Tuesday Mar 26, 2024 22:08:05 GMT
 */

#include "colours.h"
#include <alloca.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Board colours
#define WHTSQCOLOR GRNB
#define BLKSQCOLOUR BLUB
#define WHTCOLOUR BHWHT
#define BLKCOLOUR BLK
#define LABELCOLOUR CYN

// Piece idx
enum { PAWN = 1, KING = 2, QUEEN = 3, BISHOP = 4, KNIGHT = 5, ROOK = 6 };
#define BLACK 0x200

// For pieces
enum { COL = 0, ROW = 3, POFF = 6, NUMMOVES = 11 };
#define ISCAPTURED 0x400

#define MAXMOVES 32

// Who knows...
#define PIECENOTFOUND 127

// There are move types
// Game
// 	Move-history
// 	Pieces
// 		Potential-moves
typedef unsigned char byte;

enum {
	ALGMOVETYPECAPTURE	= 0x1,
	ALGMOVETYPEKINGCASTLE	= 0x2,
	ALGMOVETYPEQUEENCASTLE	= 0x4,
	ALGMOVETYPEMOVENUMBER	= 0x8,
	ALGMOVETYPESOURCE	= 0x10,
	ALGMOVETYPECHECK	= 0x20,
	ALGMOVETYPEVALID	= 0x40,
	INVALIDMOVERET		= -1
};

struct AlgMove {
	// piece is p,r,n,b,q,k // 3 bits
	char piece;
	char src; // when pawns capture this is the column it left from
	// bits 7:3 are the score of the move
	char score;

	char col; // 3 bits // dest
	char row; // 3 bits // dest
	char type;
	// # checkmate
	// + check
	// X capture
	// O-O, 0-0 Kingside castle
	// O-O-O, 0-0-0 Queenside castle
};

typedef struct AlgMove AlgMove;

struct Piece {
	// Encoding
	// numMoves[15:11] isCaptured[10] isBlack[9] piece[8:6] row[5:3], col[2:0]
	unsigned short data;
	AlgMove moves[MAXMOVES];
};

typedef struct Piece Piece;

//   a b c d e f g h
// 8   x   x   x   x 8
// 7                 7
// 6                 6
// 5                 5
// 4                 4
// 3                 3
// 2                 2
// 1                 1
//   a b c d e f g h
struct Game {
	byte numActivePieces; // last element in Piece array of a valid piece
	byte enpassantSq;     // bits: valid {7} 0'b {6} row {5:3} col {0:2}
	byte lastPMove;
	byte lastPMoveLoc;
	Piece p[32];
	AlgMove moveHistory[MAXMOVES];
	byte board[8][8]; // for printing out
};

typedef struct Game Game;

const char pieceNames[8] = {' ', 'P', 'K', 'Q', 'B', 'N', 'R'};

// ------------------- Random funcs -----------------------------

unsigned int SEED = 0x2024;
int rand_max(int max) {
	int a, c, m;
	// https://en.wikipedia.org/wiki/Linear_congruential_generator#Parameters_in_common_use
	a = 75;
	c = 74;
	m = 0x10001;
	SEED = (a * SEED + c) % m;
	return (float)max * (float)SEED / (float)0x10001;
}

char charAbs(char a) {
	char tmp = a >> 7;
	a ^= tmp;
	a -= tmp;
	return a;
}

// -------------------- End of random funcs ----------------------

// ---------------------- AlgMove funcs -----------------------------

void initAlgMove(AlgMove *move) {
	move->piece = 0;
	move->score = 0;
	move->src = -1;
	move->col = 0;
	move->row = 0;
	move->type = 0;
}

void setCaptureMove(AlgMove *move) { move->type |= ALGMOVETYPECAPTURE; }

void fprintAlgMove(FILE *fptr, AlgMove move) {
	if (!(move.type & ALGMOVETYPEVALID)) {
		fprintf(fptr, "INVALID ALG MOVE\n");
	}
	if (move.type == ALGMOVETYPEKINGCASTLE) {
		fprintf(fptr, "O-O");
	} else if (move.type == ALGMOVETYPEQUEENCASTLE) {
		fprintf(fptr, "O-O-O");
	} else if (move.type == ALGMOVETYPEMOVENUMBER) {
		fprintf(fptr, "%d.", move.piece);
	} else {
		if (move.piece != 'P') {
			fprintf(fptr, "%c", move.piece);
		} else if (move.type == ALGMOVETYPESOURCE) {
			fprintf(fptr, "%c", move.src);
		}

		if (move.type & ALGMOVETYPECAPTURE) {
			fprintf(fptr, "x");
		}
		fprintf(fptr, "%c%d", move.col + 'a', move.row + 1);
	}
	fprintf(fptr, " ");
}

void printAlgMove(AlgMove move) { fprintAlgMove(stdout, move); }

char getMVRow(AlgMove move) { return move.row + 1; }

char getMVCol(AlgMove move) { return move.col + 'a'; }

// takes algebraic move 1 indexed, i.e. e1, g7, f4
void setMVColRow(AlgMove *move, char col, byte row) {
	move->col = col - 'a';
	move->row = row - 1;
}

void setMoveScore(AlgMove *move, char score) {
	move->score = score << 3;
}

char getMoveScore(AlgMove move) {
	return move.score >> 3;
}

byte isValid(AlgMove move) { return (move.type & ALGMOVETYPEVALID); }

byte isCapture(AlgMove move) { return (move.type & ALGMOVETYPECAPTURE); }


// --------------------- End of AlgMove funcs -----------------------

// --------------------- Piece funcs -------------------------------
byte getNumMoves(Piece piece) { return piece.data >> NUMMOVES; }

void setNumMoves(Piece *piece, char numMoves) {
	// clear top 5 bits
	// printf(" data %x\n", piece->data);
	piece->data &= ~(0x1f << NUMMOVES);
	piece->data |= (numMoves << NUMMOVES);
	// printf(" data %x\n", piece->data);
}

byte isCaptured(Piece piece) { return (piece.data & ISCAPTURED) != 0; }

byte isBlack(Piece piece) { return (piece.data & BLACK) != 0; }

byte isWhite(Piece piece) { return !isBlack(piece); }

byte getPiece(Piece piece) { return (piece.data >> POFF) & 0x7; }

byte getName(Piece piece) { return pieceNames[getPiece(piece)]; }

// returns 1 indexed value i.e. 1 to 8
char getRow(Piece piece) { return (piece.data >> ROW & 0x7) + 1; }

// returns 1 indexed algebraic value i.e. 'a' to 'h'
char getCol(Piece piece) { return (piece.data >> COL & 0x7) + 'a'; }

void printPiece(Piece p) {
	byte name;
	name = pieceNames[getPiece(p)];
	printf("val %d, name: %c, col: %c, row: %d colour: %d, captured: %d, numMoves: %d\n",
			p.data, name, getCol(p), getRow(p), isBlack(p), isCaptured(p), getNumMoves(p));
}

void fprintMove(FILE *fptr, Piece p, byte moveIdx) {
	fprintf(fptr, "move: %d / %d ", moveIdx + 1, getNumMoves(p));
	fprintAlgMove(fptr, p.moves[moveIdx]);
	fprintf(fptr, "score: %d ", getMoveScore(p.moves[moveIdx]));
}

void printMove(Piece p, byte moveIdx) { fprintMove(stdout, p, moveIdx); }

void fprintMoves(FILE *fptr, Piece p) {
	byte i;
	byte numMoves;
	numMoves = getNumMoves(p);
	for (i = 0; i < numMoves; i++) {
		fprintMove(fptr, p, i);
		fprintf(fptr, "\n");
	}
}

void printMoves(Piece p) { fprintMoves(stdout, p); }

// ----------------------- End of Piece funcs ---------------------

// ------------------------ Game funcs ----------------------------

// takes algebraic coord like d4, e7, a1 etc
byte findPieceAt(Game *g, char col, char row) {
	byte i;
	if (!('a' <= col && col <= 'h')) {
		printf("\nError col: %c, invalid at findPiece\n", col);
		exit(1);
	}
	if (!(1 <= row && row <= 8)) {
		printf("\nError row: %d, invalid at findPiece\n", row);
		exit(1);
	}
	// printf("findPieceAt: %c%d\n", col, row);
	for (i = 0; i < g->numActivePieces; i++) {
		if (getRow(g->p[i]) == row && getCol(g->p[i]) == col) {
			return i;
		}
	}
	// printf("Error Piece not found at %c%d\n", col, row);
	return PIECENOTFOUND;
}

// takes char col 'a' to 'h' and 1 indexed row 1 to 8
int setPiece(Game *g, char curCol, byte curRow, char newCol, byte newRow) {
	short mask, newCoord;
	byte idx;
	idx = findPieceAt(g, curCol, curRow);
	if (idx == PIECENOTFOUND) {
		printf("Error piece not found in setPiece! %c%d -> %c%d\n", curCol, curRow,
				newCol, newRow);
		exit(1);
		return PIECENOTFOUND;
	}
	curRow = curRow - 1;
	curCol = curCol - 'a';
	g->lastPMoveLoc = curRow * 8 + curCol;
	newRow = newRow - 1;
	newCol = newCol - 'a';

	// printf("val: %x\n", val);
	// row + col is 6 bits, 3bits each
	mask = ((1 << 6) - 1) << COL;
	// printf("mask: %x\n", mask);
	g->p[idx].data &= ~mask;
	// printf("val &= ~mask: %x\n", val);
	newCoord = newRow << ROW | newCol << COL;
	// printf("coord: %x\n", newCoord);
	g->p[idx].data |= newCoord;
	// printf("new val: %x\n", val);
	return 0;
}

void updateBoard(Game *g) {
	Piece p;
	byte i, row, col;
	// clear board
	// lastMove = -1;
	for (row = 0; row < 8; row++) {
		for (col = 0; col < 8; col++) {
			g->board[row][col] = 0;
		}
	}
	for (i = 0; i < g->numActivePieces; i++) {
		p = g->p[i];
		// printf("val %d, row: %d, col: %d, colour: %d, name: %c\n",
		// 		pieces[i], row, col, isBlack, name);
		if (!isCaptured(p)) {
			g->board[getRow(p) - 1][getCol(p) - 'a'] = getPiece(p) | isBlack(p) << 3;
		}
	}
}

void printBoard(Game *g) {
	int row, col, i, isOdd;
	byte c, val;
	updateBoard(g);
	printf("  ");
	for (i = 0; i < 8; i++) {
		printf(LABELCOLOUR "%c " CRESET, 'a' + i);
	}
	printf("\n");
	for (row = 7; row >= 0; row--) {
		printf(LABELCOLOUR "%d " CRESET, row + 1);
		for (col = 0; col < 8; col++) {
			val = g->board[row][col];
			if (val & 0x8) {
				printf(BLKCOLOUR);
			} else {
				printf(WHTCOLOUR);
			}
			c = pieceNames[val & 0x7];
			isOdd = ((row + col) % 2 != 0);
			if (isOdd) {
				printf(WHTSQCOLOR);
			} else {
				printf(BLKSQCOLOUR);
			}
			if (g->lastPMoveLoc > 0 && row * 8 + col == g->lastPMoveLoc) {
				if (val & 0x8) {
					printf(YELB);
				} else {
					printf(MAGB);
				}
			}
			if (!(val & ISCAPTURED)) {
				printf("%c ", c);
			}
			printf(CRESET);
		}
		printf(LABELCOLOUR " %d\n" CRESET, row + 1);
	}
	printf("  ");
	for (i = 0; i < 8; i++) {
		printf(LABELCOLOUR "%c " CRESET, 'a' + i);
	}
	printf("\n");
}

void printBoardWithMoves(Game *g, byte pIdx) {
	char row, col, isOdd;
	byte c, val, i;
	Piece p;
	p = g->p[pIdx];

	updateBoard(g);
	printf("  ");
	for (i = 0; i < 8; i++) {
		printf(LABELCOLOUR "%c " CRESET, 'a' + i);
	}
	printf("\n");
	for (row = 8; row >= 1; row--) {
		printf(LABELCOLOUR "%d " CRESET, row);
		for (col = 'a'; col <= 'h'; col++) {
			val = g->board[row - 1][col - 'a'];
			if (val & 0x8) {
				printf(BLKCOLOUR);
			} else {
				printf(WHTCOLOUR);
			}
			c = pieceNames[val & 0x7];
			isOdd = ((row + col) % 2 != 0);
			if (isOdd) {
				printf(WHTSQCOLOR);
			} else {
				printf(BLKSQCOLOUR);
			}
			// if (lastMove > 0 && row * 8 + col == lastMove) {
			// 	printf(YELB);
			// }
			for (i = 0; i < getNumMoves(p); i++) {
				if (getMVRow(p.moves[i]) == row && getMVCol(p.moves[i]) == col) {
					printf(REDB);
				}
				if (getRow(p) == row && getCol(p) == col) {
					if (val & 0x8) {
						printf(YELB);
					} else {
						printf(MAGB);
					}
				}
			}
			if (!(val & ISCAPTURED)) {
				printf("%c ", c);
			}
			printf(CRESET);
		}
		printf(LABELCOLOUR " %d\n" CRESET, row);
	}
	printf("  ");
	for (i = 'a'; i <= 'h'; i++) {
		printf(LABELCOLOUR "%c " CRESET, i);
	}
	printf("\n");
}

void initGame(Game *g) {
	// byte numActivePieces; // last element in Piece array of a valid piece
	// byte enpassantSq; // bits: valid {7} 0'b {6} row {5:3} col {0:2}
	// byte lastPMove;
	// byte lastPMoveLoc;
	// Piece p[32];
	// AlgMove moveHistory[MAXMOVES];
	// byte board[8][8]; // for printing out

	g->numActivePieces = 32;
	g->enpassantSq = 0;
	g->lastPMove = 0;
	g->lastPMoveLoc = -1;

	byte col;
	short i;
	i = 0;

	// White pawns
	for (col = 0; col < 8; col++) {
		g->p[i++].data = 1 << ROW | col << COL | PAWN << POFF;
	}
	// White pieces
	g->p[i++].data = 0 << ROW | 0 << COL | ROOK << POFF;
	g->p[i++].data = 0 << ROW | 7 << COL | ROOK << POFF;
	g->p[i++].data = 0 << ROW | 1 << COL | KNIGHT << POFF;
	g->p[i++].data = 0 << ROW | 6 << COL | KNIGHT << POFF;
	g->p[i++].data = 0 << ROW | 2 << COL | BISHOP << POFF;
	g->p[i++].data = 0 << ROW | 5 << COL | BISHOP << POFF;
	g->p[i++].data = 0 << ROW | 3 << COL | QUEEN << POFF;
	g->p[i++].data = 0 << ROW | 4 << COL | KING << POFF;

	// Black Pawns
	for (col = 0; col < 8; col++) {
		g->p[i++].data = 6 << ROW | col << COL | PAWN << POFF | BLACK;
	}
	// Black pieces
	g->p[i++].data = 7 << ROW | 0 << COL | ROOK << POFF | BLACK;
	g->p[i++].data = 7 << ROW | 7 << COL | ROOK << POFF | BLACK;
	g->p[i++].data = 7 << ROW | 1 << COL | KNIGHT << POFF | BLACK;
	g->p[i++].data = 7 << ROW | 6 << COL | KNIGHT << POFF | BLACK;
	g->p[i++].data = 7 << ROW | 2 << COL | BISHOP << POFF | BLACK;
	g->p[i++].data = 7 << ROW | 5 << COL | BISHOP << POFF | BLACK;
	g->p[i++].data = 7 << ROW | 3 << COL | QUEEN << POFF | BLACK;
	g->p[i++].data = 7 << ROW | 4 << COL | KING << POFF | BLACK;

	for (i = 0; i < MAXMOVES; i++) {
		initAlgMove(g->moveHistory);
	}
	updateBoard(g);
}

int intersects(byte startRow, byte startCol, byte endRow, byte endCol,
		byte checkRow, byte checkCol) {
	char r, c;
	char dr, dc;

	dr = endRow - startRow;
	dc = endCol - startCol;

	if (dr < 0) {
		dr = -1;
	} else if (dr > 0) {
		dr = 1;
	}

	if (dc < 0) {
		dc = -1;
	} else if (dc > 0) {
		dc = 1;
	}

	r = startRow;
	c = startCol;
	while (r != endRow || c != endCol) {
		if (r == checkRow && c == checkCol) {
			return 1;
		}
		r += dr;
		c += dc;
	}
	return 0;
}

int checkIntersects(Game *g, byte pIdx, byte moveIdx) {
	byte row, col, j, intersect;
	// now check which of these moves are actually valid
	// compare with each piece
	Piece p;
	AlgMove move;
	p = g->p[pIdx];
	move = g->p[pIdx].moves[moveIdx];
	row = getMVRow(move);
	col = getMVCol(move);
	intersect = 0;
	// for every piece on the board
	for (j = 0; j < g->numActivePieces; j++) {
		if (isCaptured(g->p[j]) || j == pIdx) {
			continue;
		}

		intersect = intersects(getRow(p), getCol(p), row, col, getRow(g->p[j]),
				getCol(g->p[j]));
		if (intersect) {
			return 1;
		}
	}
	return 0;
}

int checkMove(Game *g, byte pIdx, byte moveIdx) {
	byte j, row, col;
	Piece p, pj;
	AlgMove move;

	p = g->p[pIdx];
	move = p.moves[moveIdx];
	row = getMVRow(move);
	col = getMVCol(move);

	// if knight we can skip intersection check
	if ((getPiece(p) != KNIGHT) && checkIntersects(g, pIdx, moveIdx)) {
		return 0;
	}

	// for every piece on the board
	for (j = 0; j < g->numActivePieces; j++) {
		pj = g->p[j];
		if (isCaptured(pj) || j == pIdx) {
			continue;
		}

		// if we didn't intersect the move might be valid, need to
		// check if the end square is free
		if ((row == getRow(pj)) && (col == getCol(pj))) {
			// because the same colour can't capture
			if (isBlack(p) == isBlack(pj)) {
				return INVALIDMOVERET;
			}
			// if a pawn tries to capture non-diagonally
			if ((getPiece(p) == PAWN) && !isCapture(move)) {
				return INVALIDMOVERET;
			}
			if (getPiece(pj) == KING) {
				return ALGMOVETYPECHECK;
			}
			return ALGMOVETYPECAPTURE;
		}
	}
	// pawn only capture move but we did not actually capture
	if ((getPiece(p) == PAWN) && isCapture(move)) {
		// if en-passant we return a capture move
		// if() {
		// }
		return INVALIDMOVERET;
	}
	return ALGMOVETYPEVALID;
}

int checkValidMoves(Game *g, byte pIdx, byte numberOfMoves) {
	byte i;
	// now check which of these moves are actually valid
	char overlap;
	Piece *p;
	AlgMove *move;
	p = &g->p[pIdx];

	for (i = 0; i < numberOfMoves; i++) {
		move = &p->moves[i];
		overlap = checkMove(g, pIdx, i);
		if (overlap == ALGMOVETYPEVALID) {
			move->type = ALGMOVETYPEVALID;
		} else if (overlap == ALGMOVETYPECAPTURE || overlap == ALGMOVETYPECHECK) {
			move->type = ALGMOVETYPEVALID | ALGMOVETYPECAPTURE;
		} else { // INVALIDMOVE
			move->type &= ~ALGMOVETYPEVALID;
		}
	}

	byte oldNumMoves = numberOfMoves;
	numberOfMoves = 0;
	for (i = 0; i < oldNumMoves; i++) {
		move = &p->moves[i];
		if (isValid(*move)) {
			p->moves[numberOfMoves++] = *move;
		}
	}
	setNumMoves(p, numberOfMoves);
	return numberOfMoves;
}

int pawnMoves(Game *g, byte idx) {
	byte numMoves, i;
	char row, col;
	Piece *p;
	AlgMove *moves;
	p = &g->p[idx];
	moves = &p->moves[0];

	row = getRow(*p);
	col = getCol(*p);
	numMoves = getNumMoves(*p);

	if (!isBlack(*p)) {
		setMVColRow(&moves[numMoves++], col, row + 1);
		if (row == 2) { // i.e. not moved
			setMVColRow(&moves[numMoves++], col, row + 2);
		}
		// check capture moves
		if (col < 'h') {
			setCaptureMove(&moves[numMoves]);
			setMVColRow(&moves[numMoves++], col + 1, row + 1);
		}
		if (col > 'a') {
			setCaptureMove(&moves[numMoves]);
			setMVColRow(&moves[numMoves++], col - 1, row + 1);
		}
	} else { // BLACK
		setMVColRow(&moves[numMoves++], col, row - 1);
		if (row == 7) { // i.e. not moved
			setMVColRow(&moves[numMoves++], col, row - 2);
		}
		// check capture moves
		if (col < 'h') {
			setCaptureMove(&moves[numMoves]);
			setMVColRow(&moves[numMoves++], col + 1, row - 1);
		}
		if (col > 'a') {
			setCaptureMove(&moves[numMoves]);
			setMVColRow(&moves[numMoves++], col - 1, row - 1);
		}
	}

	setNumMoves(p, numMoves);
	// printf("ALL POSSIBLE MOVES\n");
	// printf("======================\n");
	// printMoves(*p);
	// printf("======================\n");

	numMoves = checkValidMoves(g, idx, numMoves);
	setNumMoves(p, numMoves);
	return numMoves;
}

int rookMoves(Game *g, byte idx) {
	byte numMoves;
	char col, row;
	char i;
	Piece *p;
	AlgMove *moves;
	p = &g->p[idx];
	moves = &p->moves[0];

	row = getRow(*p);
	col = getCol(*p);

	numMoves = getNumMoves(*p);
	// right
	for (i = 1; col + i <= 'h'; i++) {
		setMVColRow(&moves[numMoves++], col + i, row);
	}
	// left
	for (i = 1; col - i >= 'a'; i++) {
		setMVColRow(&moves[numMoves++], col - i, row);
	}
	// down
	for (i = 1; row - i >= 1; i++) {
		setMVColRow(&moves[numMoves++], col, row - i);
	}
	// up
	for (i = 1; row + i <= 8; i++) {
		setMVColRow(&moves[numMoves++], col, row + i);
	}

	setNumMoves(p, numMoves);
	numMoves = checkValidMoves(g, idx, numMoves);
	setNumMoves(p, numMoves);
	return numMoves;
}

int bishopMoves(Game *g, byte idx) {
	char col, row, i;
	byte numMoves;
	Piece *p;
	AlgMove *moves;
	p = &g->p[idx];
	moves = &p->moves[0];

	row = getRow(*p);
	col = getCol(*p);

	numMoves = getNumMoves(*p);
	// printf("b num moves start %d\n", numMoves);
	// up-right
	for (i = 1; ((row + i) <= 8) && ((col + i) <= 'h'); i++) {
		setMVColRow(&moves[numMoves++], col + i, row + i);
	}
	// down-right
	for (i = 1; ((row - i) >= 1) && ((col + i) <= 'h'); i++) {
		setMVColRow(&moves[numMoves++], col + i, row - i);
	}
	// down-left
	for (i = 1; ((row - i) >= 1) && ((col - i) >= 'a'); i++) {
		setMVColRow(&moves[numMoves++], col - i, row - i);
	}
	// up-left
	for (i = 1; ((row + i) <= 8) && ((col - i) >= 'a'); i++) {
		setMVColRow(&moves[numMoves++], col - i, row + i);
	}

	setNumMoves(p, numMoves);
	numMoves = checkValidMoves(g, idx, numMoves);
	// printf("b num moves end %d\n", numMoves);
	// printf("getNumMoves(g->p[idx]) %d\n", getNumMoves(g->p[idx]));
	setNumMoves(p, numMoves);
	// printf("getNumMoves(g->p[idx]) %d\n", getNumMoves(g->p[idx]));
	return numMoves;
}

int knightMoves(Game *g, byte idx) {
	char col, row;
	byte numMoves;
	Piece *p;
	AlgMove *moves;
	p = &g->p[idx];
	moves = &p->moves[0];
	row = getRow(*p);
	col = getCol(*p);

	numMoves = getNumMoves(*p);

	// L is row +/-1 col +/-2
	// 	row +/-2 col +/-1
	if (col >= 'c' && row >= 2) {
		setMVColRow(&moves[numMoves++], col - 2, row - 1);
	}
	if (col >= 'c' && row <= 7) {
		setMVColRow(&moves[numMoves++], col - 2, row + 1);
	}
	if (col <= 'f' && row >= 2) {
		setMVColRow(&moves[numMoves++], col + 2, row - 1);
	}
	if (col <= 'f' && row <= 7) {
		setMVColRow(&moves[numMoves++], col + 2, row + 1);
	}
	if (row >= 3 && col >= 'b') {
		setMVColRow(&moves[numMoves++], col - 1, row - 2);
	}
	if (row >= 3 && col <= 'g') {
		setMVColRow(&moves[numMoves++], col + 1, row - 2);
	}
	if (row <= 6 && col >= 'b') {
		setMVColRow(&moves[numMoves++], col - 1, row + 2);
	}
	if (row <= 6 && col <= 'g') {
		setMVColRow(&moves[numMoves++], col + 1, row + 2);
	}

	setNumMoves(p, numMoves);
	numMoves = checkValidMoves(g, idx, numMoves);
	setNumMoves(p, numMoves);
	return numMoves;
}

int kingMoves(Game *g, byte idx) {
	char col, row;
	byte numMoves;
	Piece *p;
	AlgMove *moves;
	p = &g->p[idx];
	moves = &p->moves[0];
	row = getRow(*p);
	col = getCol(*p);

	numMoves = getNumMoves(*p);

	// down
	if (row >= 2) {
		setMVColRow(&moves[numMoves++], col, row - 1);
	}
	// down-left
	if (col >= 'b' && row >= 2) {
		setMVColRow(&moves[numMoves++], col - 1, row - 1);
	}
	// left
	if (col >= 'b') {
		setMVColRow(&moves[numMoves++], col - 1, row);
	}
	// up-left
	if (col >= 'b' && row <= 7) {
		setMVColRow(&moves[numMoves++], col - 1, row + 1);
	}
	// up
	if (row <= 7) {
		setMVColRow(&moves[numMoves++], col, row + 1);
	}
	// up-right
	if (col <= 'g' && row <= 7) {
		setMVColRow(&moves[numMoves++], col + 1, row + 1);
	}
	// right
	if (col <= 'g') {
		setMVColRow(&moves[numMoves++], col + 1, row);
	}
	// down-right
	if (col <= 'g' && row >= 2) {
		setMVColRow(&moves[numMoves++], col + 1, row - 1);
	}

	setNumMoves(p, numMoves);
	numMoves = checkValidMoves(g, idx, numMoves);
	setNumMoves(p, numMoves);
	return numMoves;
}

// sorts the moves of a piece based on the score for each move
// the scores are set in scoreMoves
void sortMoves(Piece *p) {
	byte i, j, numMoves;
	AlgMove tmp;
	numMoves = getNumMoves(*p);

	for (i = 0; i < numMoves; i++) {
		j = i;
		tmp = p->moves[i];
		while ((j > 0) && (getMoveScore(p->moves[j-1]) < getMoveScore(tmp))) {
			// set the score of move j to the larger score of j-1
			p->moves[j] = p->moves[j-1];
			j--;
		}
		p->moves[j] = tmp;
	}
}

// scores the moves of a piece, then sorts the moves in order of best to worst
// score
void scorePieceMoves(Piece *p) {
	byte i, numMoves, score;
	AlgMove move;
	numMoves = getNumMoves(*p);
	for (i = 0; i < numMoves; ++i) {
		score = 0;
		move = p->moves[i];
		if (isCapture(move)) {
			score += 2;
		}
		setMoveScore(&p->moves[i], score);
	}
	sortMoves(p);
}

// search through the board for all possible pieces that can move
int possibleMoves(Game *g, byte idx) {
	byte piece, i;
	Piece *p;
	// if square is empty
	piece = getPiece(g->p[idx]);
	p = &g->p[idx];
	setNumMoves(p, 0);
	if (piece == PAWN) {
		// check forward moves
		// diagonal captures
		// if not moved check 2 squares
		pawnMoves(g, idx);
		for (i = 0; i < getNumMoves(*p); ++i) {
			p->moves[i].piece = 'P';
		}
	} else if (piece == ROOK) {
		// check up down left right
		rookMoves(g, idx);
		for (i = 0; i < getNumMoves(*p); ++i) {
			p->moves[i].piece = 'R';
		}
	} else if (piece == KNIGHT) {
		// check L
		knightMoves(g, idx);
		for (i = 0; i < getNumMoves(*p); ++i) {
			p->moves[i].piece = 'N';
		}
	} else if (piece == BISHOP) {
		// check diagonals
		bishopMoves(g, idx);
		for (i = 0; i < getNumMoves(*p); ++i) {
			p->moves[i].piece = 'B';
		}
	} else if (piece == QUEEN) {
		// check up down left right
		// diagonals
		rookMoves(g, idx);
		// printf("num moves rook %d\n", getNumMoves(g->p[idx]));
		bishopMoves(g, idx);
		// printf("num moves bishop %d\n", getNumMoves(g->p[idx]));
		for (i = 0; i < getNumMoves(*p); ++i) {
			p->moves[i].piece = 'Q';
		}
	} else if (piece == KING) {
		// check 1 sq up down left right
		// 1 sq diag
		kingMoves(g, idx);
		for (i = 0; i < getNumMoves(*p); ++i) {
			p->moves[i].piece = 'K';
		}
	}
	scorePieceMoves(p);
	// only valid if move doesn't put you in check
	return 0;
}

void calcMoves(Game *g)
{
	byte i;
	for (i = 0; i < g->numActivePieces; i++) {
		possibleMoves(g, i);
	}
}

// pieces that can capture have a higher move score
// pieces in the centre have a higher score
// pieces that have not moved for a while have a higher score
void scoreMoves(Game *g, char *scores) {
	byte i, numMoves, mIdx;
	Piece *p;
	AlgMove move;

	// char scores[g->numActivePieces];
	for (i = 0; i < g->numActivePieces; i++) {
		p = &g->p[i];
		scorePieceMoves(p);
	}
}

void printBestMoves(Game *g)
{
	byte i, j;
	byte maxScore, score;
	maxScore = 0;
	for (i = 0; i < g->numActivePieces; ++i) {
		for (j = 0; j < getNumMoves(g->p[i]); j++) {
			score = getMoveScore(g->p[i].moves[j]);
			maxScore = (maxScore > score) ? maxScore : score;
		}
	}

	for (i = 0; i < g->numActivePieces; ++i) {
		for (j = 0; j < getNumMoves(g->p[i]); j++) {
			score = getMoveScore(g->p[i].moves[j]);
			if (score >= maxScore) {
				printMove(g->p[i], j);
				printf("\n");
			}
		}
	}
}

void playMove(Game *g, byte pIdx, byte moveIdx) {
	byte newCoord, newRow, newCol, prevIdx;
	short mask;
	Piece p;
	AlgMove move;
	p = g->p[pIdx];
	move = g->p[pIdx].moves[moveIdx];

	g->lastPMoveLoc = getRow(p) * 8 + getCol(p);
	newRow = getMVRow(move);
	newCol = getMVCol(move);

	prevIdx = findPieceAt(g, newCol, newRow);
	if (prevIdx != PIECENOTFOUND) {
		g->p[prevIdx].data |= ISCAPTURED;
		p = g->p[prevIdx];
		g->p[prevIdx] = g->p[g->numActivePieces];
		g->p[g->numActivePieces] = p;
		printf("Prev piece print ");
		printPiece(g->p[prevIdx]);
		printf("Last active piece print ");
		printPiece(g->p[g->numActivePieces]);
		g->numActivePieces--;
		printf("%d pieces left\n", g->numActivePieces);
	}

	move = g->p[pIdx].moves[moveIdx];
	mask = ((1 << 6) - 1) << COL;
	// printf("mask: %x\n", mask);
	g->p[pIdx].data &= ~mask;
	// printf("val &= ~mask: %x\n", val);
	newCoord = (getMVRow(move) - 1) << ROW | (getMVCol(move) - 'a') << COL;
	// printf("coord: %x\n", newCoord);
	g->p[pIdx].data |= newCoord;
}

// ---------------- End of Game funcs ------------------------

// --------------------- PGN parsing funcs ------------------
char isARow(char c) { return ('a' <= c && c <= 'h'); }

char isACaptureMove(char c) { return (c == 'x' || c == 'X'); }

char strToNumber(const char *str) {
	char result, strLen;
	byte i;
	// printf("strToNumber: %s", str);
	strLen = strlen(str);
	result = 0;
	for (i = 0; i < strLen; i++) {
		if ('0' <= str[i] && str[i] <= '9') {
			result *= 10;
			result += (str[i] - '0');
		} else {
			return result;
		}
	}
	return result;
}

void parsePGNMove(AlgMove *move, const char *str) {
	char strLen;
	initAlgMove(move);
	strLen = strlen(str);
	if (strLen <= 0) {
		move->type &= ~ALGMOVETYPEVALID;
		return;
	}
	if (isARow(str[0])) { // pawn move
		move->piece = 'P';
		move->col = str[0];
		if (strLen == 2) {
			move->row = str[1];
		} else if (strLen >= 4) {
			move->type = ALGMOVETYPESOURCE;
			move->src = str[0];
			if (isACaptureMove(str[1])) {
				setCaptureMove(move);
			} else {
				printf("Error not having a good time working out this move: %s\n", str);
				move->type &= ~ALGMOVETYPEVALID;
				return;
			}
			move->col = str[2];
			move->row = str[3];
		}
	} else if (str[0] == 'R' || str[0] == 'N' || str[0] == 'B' || str[0] == 'Q' ||
			str[0] == 'K') {
		// printf("Str: %s ", str);
		move->piece = str[0];
		if (strLen >= 4) {
			if (isACaptureMove(str[1])) {
				setCaptureMove(move);
			} else {
				move->type = ALGMOVETYPESOURCE;
				move->src = str[1];
			}
			move->col = str[2];
			move->row = str[3];
		} else if (strLen == 3) {
			move->col = str[1];
			move->row = str[2];
		}
	} else if ('0' <= str[0] && str[0] <= '9') {

		if (str[0] == '0') { // castling time
			if (strLen == 3) {
				move->type = ALGMOVETYPEKINGCASTLE;
			} else if (strLen == 5) {
				move->type = ALGMOVETYPEQUEENCASTLE;
			} else {
				printf("Error not having a good time working out this move: %s\n", str);
				move->type &= ~ALGMOVETYPEVALID;
				return;
			}
		} else {
			// it is the move number
			move->type = ALGMOVETYPEMOVENUMBER;
			move->piece = strToNumber(str);
		}
	} else if (str[0] == 'O') { // castling time
		if (strLen == 3) {
			move->type = ALGMOVETYPEKINGCASTLE;
		} else if (strLen == 5) {
			move->type = ALGMOVETYPEQUEENCASTLE;
		} else {
			printf("Error not having a good time working out this move: %s\n", str);
			move->type &= ~ALGMOVETYPEVALID;
			return;
		}
	} else {
		printf("Error still not having a good time parsing whatever this is "
				"supposed to be: %s\n",
				str);
		move->type &= ~ALGMOVETYPEVALID;
		return;
	}
	move->type |= ALGMOVETYPEVALID;
	return;
}

void parsePGN(const char *filename) {
	char output[1024][8];
	char buf[1024];
	FILE *fptr;
	int outputLoc, tokLen, i;
	const char *str;

	outputLoc = 0;
	fptr = fopen(filename, "r");
	if (fptr == NULL) {
		printf("Error not a valid filename!\n");
		return;
	}
	while (fgets(buf, sizeof(buf), fptr)) {
		printf("%s\n", buf);
		// split buf on spaces
		char *tok = strtok(buf, " ");
		while (tok != NULL) {
			// printf("tok: %s\n", tok);
			tokLen = strlen(tok);
			strncpy(output[outputLoc++], tok, tokLen);
			tok = strtok(NULL, " ");
		}
	}
	fclose(fptr);

	AlgMove move;
	FILE *fptrout;
	fptrout = fopen("testGame1Parse.txt", "w");
	for (i = 0; i < outputLoc; i++) {
		str = output[i];
		if ((i == outputLoc - 1) && str[1] == '-') {
			continue; // last move
		}
		parsePGNMove(&move, str);
		// printf("%s\n", output[i]);
		printAlgMove(move);
		fprintAlgMove(fptrout, move);
	}
	fclose(fptrout);
	printf("\n");
}

// ------------------------------- End of PGN parsing funcs ------------------

void testRandomMoves(Game *g) {
	byte c;
	int i;
	Piece *p;

	initGame(g);
	for (c = 'a'; c <= 'h'; c++) {
		if (c % 2 == 0) {
			setPiece(g, c, 2, c, 2 + 1);
			setPiece(g, c, 7, c, 5);
		} else {
			setPiece(g, c, 2, c, 2 + 2);
			setPiece(g, c, 7, c, 6);
		}
	}

	// setPiece(pieces, 'a', 1, 'b', 4);
	// setPiece(pieces, 'd', 2, 'd', 6);
	printBoard(g);
	for (i = 0; i < g->numActivePieces; i++) {
		possibleMoves(g, i);
		// printBoardWithMoves(g, i);
	}

	byte whiteTurn = 1;
	int rPiece, rMove;
	initGame(g);
	printBoard(g);
	int maxTurns = 128;
	FILE *fptr;
	fptr = fopen("game.log", "w");
	for (i = 0; i < maxTurns; i++) {
		// pick a random piece
		// then pick a random move
		if (whiteTurn) {
			fprintf(fptr, "\n%d. ", i / 2);
		}
		rPiece = rand_max(g->numActivePieces);
		possibleMoves(g, rPiece);
		while ((isWhite(g->p[rPiece]) != whiteTurn) ||
				(getNumMoves(g->p[rPiece]) == 0)) {
			rPiece = rand_max(g->numActivePieces);
			p = &g->p[rPiece];
			printf("rPiece: %d\n", rPiece);
			possibleMoves(g, rPiece);
			// printPiece(*p);
			// printBoardWithMoves(g, rPiece);
			// fprintMoves(fptr, g->p[rPiece]);
		}
		printBoardWithMoves(g, rPiece);
		rMove = rand_max(getNumMoves(g->p[rPiece]));
		printf("Move Number %d = ", i / 2);
		printMove(g->p[rPiece], rMove);
		printf("\n");
		playMove(g, rPiece, rMove);
		printBoard(g);
		fprintMove(fptr, g->p[rPiece], rMove);
		whiteTurn = !whiteTurn;
	}
}

void testBestMoves(Game *g)
{
	char c;
	initGame(g);
	for (c = 'a'; c <= 'h'; c++) {
		if (c % 2 == 0) {
			setPiece(g, c, 2, c, 2 + 1);
			setPiece(g, c, 7, c, 5);
		} else {
			setPiece(g, c, 2, c, 2 + 2);
			setPiece(g, c, 7, c, 6);
		}
	}

	printBoard(g);

	calcMoves(g);
	printBestMoves(g);

}

int main(int argc, char *argv[]) {
	Game *g = malloc(sizeof(*g));
	initGame(g);
	printBoard(g);

	int i;
	const char *str;
	for (i = 1; i < argc; i++) {
		str = argv[i];
		printf("arg[%d]: %s\n", i, str);
		if (strncmp(str, "-h", 2) == 0 || strncmp(str, "--help", 6) == 0) {
			printf("help\n");
		} else if (strncmp(str, "-l", 2) == 0 || strncmp(str, "--log", 5) == 0) {
			printf("log\n");
		} else { // open filename
			parsePGN(str);
		}

		// isWhite = move(pieces, str, isWhite);
		printBoard(g);
	}
	// testRandomMoves(g);
	// printBoard(pieces);

	testBestMoves(g);


	free(g);
	return 0;
}
