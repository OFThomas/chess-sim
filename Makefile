######################################################################
# @author      : oli (oli@$HOSTNAME)
# @file        : Makefile
# @created     : Wednesday Mar 27, 2024 19:03:38 GMT
######################################################################

IDIR =./include
CC=gcc
CFLAGS=-I$(IDIR) -Wall -Wextra -pedantic -fsanitize=address,undefined

ODIR=obj

LIBS=

_OBJ = main.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: %.c
	mkdir -p $(ODIR)
	$(CC) -c -o $@ $< $(CFLAGS)

main: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o

