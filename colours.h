/**
 * @author      : oli (oli@$HOSTNAME)
 * @file        : colours
 * @created     : Tuesday Apr 02, 2024 23:35:14 BST
 */

#ifndef COLOURS_H

#define COLOURS_H

//Regular text
#define BLK "\33[0;30m"
#define RED "\33[0;31m"
#define GRN "\33[0;32m"
#define YEL "\33[0;33m"
#define BLU "\33[0;34m"
#define MAG "\33[0;35m"
#define CYN "\33[0;36m"
#define WHT "\33[0;37m"

//Regular bold text
#define BBLK "\33[1;30m"
#define BRED "\33[1;31m"
#define BGRN "\33[1;32m"
#define BYEL "\33[1;33m"
#define BBLU "\33[1;34m"
#define BMAG "\33[1;35m"
#define BCYN "\33[1;36m"
#define BWHT "\33[1;37m"

//Regular underline text
#define UBLK "\33[4;30m"
#define URED "\33[4;31m"
#define UGRN "\33[4;32m"
#define UYEL "\33[4;33m"
#define UBLU "\33[4;34m"
#define UMAG "\33[4;35m"
#define UCYN "\33[4;36m"
#define UWHT "\33[4;37m"

//Regular background
#define BLKB "\33[40m"
#define REDB "\33[41m"
#define GRNB "\33[42m"
#define YELB "\33[43m"
#define BLUB "\33[44m"
#define MAGB "\33[45m"
#define CYNB "\33[46m"
#define WHTB "\33[47m"

//High intensty background
#define BLKHB "\33[0;100m"
#define REDHB "\33[0;101m"
#define GRNHB "\33[0;102m"
#define YELHB "\33[0;103m"
#define BLUHB "\33[0;104m"
#define MAGHB "\33[0;105m"
#define CYNHB "\33[0;106m"
#define WHTHB "\33[0;107m"

//High intensty text
#define HBLK "\33[0;90m"
#define HRED "\33[0;91m"
#define HGRN "\33[0;92m"
#define HYEL "\33[0;93m"
#define HBLU "\33[0;94m"
#define HMAG "\33[0;95m"
#define HCYN "\33[0;96m"
#define HWHT "\33[0;97m"

//Bold high intensity text
#define BHBLK "\33[1;90m"
#define BHRED "\33[1;91m"
#define BHGRN "\33[1;92m"
#define BHYEL "\33[1;93m"
#define BHBLU "\33[1;94m"
#define BHMAG "\33[1;95m"
#define BHCYN "\33[1;96m"
#define BHWHT "\33[1;97m"

//Reset
#define reset "\033[0m"
#define CRESET "\033[0m"
#define COLOR_RESET "\033[0m"

#endif /* end of include guard COLOURS_H */

